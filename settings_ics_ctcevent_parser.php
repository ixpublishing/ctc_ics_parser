<?php 
echo "hello, world!";
?>
<form method="POST" enctype="multipart/form-data">
    <label for="select_ics">Select ICS File</label>
    <input type="file" name="select_ics" id="select_ics">
    <select name="select_post_status">
        <option selected value="draft">Draft</option>
        <option value="publish">Published</option> 
    </select>
    <input type="submit" name="submit" class="button button-primary button-large">
</form>

<?php 
if (isset($_POST["submit"])) {
    require 'class.iCalReader.php';

    $file = $_FILES["select_ics"]["tmp_name"];  //tmp_name is the temporary file that is uploaded
    //THEN: parse ICS into object
    $ical = new ICal($file);

    if ($ical->cal != NULL) {
        //test to make sure this is actually a ics file we've parsed. If it isn't, $ical->cal is null.
        print("Number of Events: " . $ical->event_count . "<br/>");
        print("Number of TODOs: " . $ical->todo_count . "<br/>");

        $events = $ical->events();

        foreach ($events as $event) {
            print("<p><div class='event'>");
            print("inserting event...<br/>");
            print("status is: " . $_POST["select_post_status"]);
            print_event_line($event, "Summary: ", "<br/>", "SUMMARY");
            print_event_line($event, "DTStart: ", "<br/>", "DTSTART");
            print_event_line($event, "DTEnd: ", "<br/>", "DTEND");
            print_event_line($event, "DTStamp: ", "<br/>", "DTSTAMP");
            print_event_line($event, "UID: ", "<br/>", "UID");
            print_event_line($event, "Created: ", "<br/>", "CREATED");
            print_event_line($event, "Description: ", "<br/>", "DESCRIPTION");
            print_event_line($event, "Last Modified: ", "<br/>", "LAST-MODIFIED");
            print_event_line($event, "Location: ", "<br/>", "LOCATION");
            print_event_line($event, "Sequence: ", "<br/>", "SEQUENCE");
            print_event_line($event, "Status: ", "<br/>", "STATUS");
            print_event_line($event, "Transp: ", "<br/>", "TRANSP");
            print("</div></p>");

            $summary = $event["SUMMARY"];
            $description = $event["DESCRIPTION"];
            $dtStart = $event["DTSTART"];
			$dtEnd = $event["DTEND"];
            $last_modified = 0;
            if (array_key_exists("LAST-MODIFIED", $event)) 
                $last_modified = $event["LAST-MODIFIED"];

			$unix_start = $ical->iCalDateToUnixTimestamp($dtStart);
			$unix_end = $ical->iCalDateToUnixTimestamp($dtEnd);
			$unix_lastedit = $ical->iCalDateToUnixTimestamp($last_modified);

            $date_start = date("Y-m-d", $unix_start);
			$date_end = date("Y-m-d", $unix_end);

			$time_start = date("g-i-s", $unix_start);
			$time_end = date("g-i-s", $unix_end);

			$date_lastedit = date("Y-m-d", $unix_lastedit);

            $id = wp_insert_post(array(
                "post_date" => $date_lastedit,
                "post_content" => $description,
                "post_title" => $summary,
                "post_type" => "ctc_event",
                "post_modified" => $date_lastedit,
                "post_status" => $_POST["select_post_status"]
            ), true);

			if ($id > 0) {
				//we want to check to make sure the post is actually valid. If it isn't, then we'll certainly throw an error when using the post_id.
				//_ctc_event_start_date
				//_Ctc_event_end_date
				//_ctc_event_start_time
				//_ctc_event_end_time
				//_ctc_event_hide_time_range
				//_ctc_event_time
				//_ctc_event_recurrance
				//_ctc_event_recurrance_weekly_interval
				//_ctc_event_recurrance_weekly_type
				//_ctc_event_recurrance_weekly_day
				//_ctc_event_recurrance_monthly_interval
				//_ctc_event_recurrance_monthly_type
				//_ctc_event_recurrance_monthly_week
				//_ctc_event_recurrance_end_date
				//_ctc_event_excluded_dates
				//_ctc_event_venue
				//_ctc_event_address
				//_ctc_event_show_directions_link
				//_ctc_event_map_lat
				//_ctc_event_map_lng
				//_ctc_event_map_type																				
				add_post_meta($id, "_ctc_event_start_date", $date_start, true);
				add_post_meta($id, "_ctc_event_end_date", $date_end, true);
				add_post_meta($id, "_ctc_event_start_time", $time_start, true);
				add_post_meta($id, "_ctc_event_end_time", $time_end, true);
				add_post_meta($id, "_ctc_event_hide_time_range", "value", true);
				add_post_meta($id, "_ctc_event_time", "value", true);
				add_post_meta($id, "_ctc_event_recurrance", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_weekly_interval", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_weekly_type", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_weekly_day", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_monthly_interval", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_monthly_type", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_monthly_week", "value", true);
				add_post_meta($id, "_ctc_event_recurrance_end_date", "value", true);
				add_post_meta($id, "_ctc_event_excluded_dates", "value", true);
				add_post_meta($id, "_ctc_event_venue", "value", true);
				add_post_meta($id, "_ctc_event_address", "value", true);
				add_post_meta($id, "_ctc_event_show_directions_link", "value", true);
				add_post_meta($id, "_ctc_event_map_lat", "value", true);
				add_post_meta($id, "_ctc_event_map_lng", "value", true);
				add_post_meta($id, "_ctc_event_map_type", "ROADMAP", true);

				var_dump($id);
	            print("Your event post id is $id.");			
			}
			else print("There was an error processing the ics file. Error: $id.");	//$id will be WP_Error in this case.
        }
    }
}

function print_event_line($event, $pretext, $posttext, $line) {
    if (array_key_exists($line, $event)) {
        print($pretext . $event[$line] . $posttext);
    }
    else print("line " . $line . " does not exist<br/>");
}
?>
