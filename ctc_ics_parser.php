<?php 
/**
 * Plugin Name: ctc ics Parser
 * Description: A plugin to parse ics files and turn them into event posts for ctc.
 * Version: 1.0.4
 *
 */

function plugin_activate() { 
    if (!post_type_exists("ctc_event")) {
        //if the post type doesn't yet exist, make sure to register it
        //Note that we do not unregister this because it is used primarily by ctc, which we should have
        //or at least eventually install.
        register_post_type("ctc_event", ['public' => 'true'] );
    }    
}	

//register_activation_hook(__FILE__, "plugin_activate");

function create_page() {
    add_menu_page("ctc ics Parser", "ctc ics Parser", "edit_posts", "ctc-ics-parser", "page_callback", "", 24);
}

add_action("admin_menu", "create_page");

function page_callback() {
    include "settings_ics_ctcevent_parser.php";
}
?>
